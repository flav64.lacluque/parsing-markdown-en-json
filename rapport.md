# Parsing Markdown en JSON

J'ai choisis d'implémenter le parseur en Python (j'ai commencé en JAVA mais j'ai vite rencontré des problèmes donc j'ai décidé de le faire en python car c'est un langage moins typé donc plus facile pour implémenter des algos).

## Utilisation de mon programme

Pour utiliser mon programme il faut exécuter mon fichier python avec comme argument le chemin vers le fichier markdown à convertir. 

Exemple :
- main.py fichier_a_convertir.md

Cela produira le fichier JSON "fichier_a_convertir.json" (en le créant si il existe pas ou en écrasant ce qu'il y a dedans s'il existe)

## Explication de l'Algo 

J'ai définis plusieurs états (PARAGRAPHE, CODE, TITRE1 ..), je parcours mon fichier markdown jusqu'à la fin ligne par ligne, au début je vérifie si ma ligne comporte des caractères spéciaux comme par exemple des guillemets. Ensuite je détermine l'état dans lequel je suis (par rapport au premier caractère de ma ligne et aussi l'état précédent), et en fonction de l'état dans lequel je suis j'ai écris les différentes fonctions pour écrire un objet JSON (ecrisCode, ecrisParagraphe ...).

J'ai différents compteurs durant le parsing pour pouvoir avoir plusieurs objets de mêmes types :  <br>
Pour les paragraphes : p1, p2... <br>
Pour les titres : h1_1, h1_2 ...  <br>
En sachant qu'on peut avoir une infinité d'objet.
J'ai également un compteur pour les accolades, pour pouvoir les fermés à la fin du parsing.

## Les erreurs remontés

Je vérifie au tout début que le fichier passé en argument est bien un fichier markdown.
J'ai un état "ERREUR" et dès que je rentre dedans je stop le parsing et je soulève l'erreur dans les logs. 
J'ai traité les erreurs suivantes :
- toujours avoir un titre de niveau 1 au commencement (à la première ligne ou à partir de X lignes vides)
- pas possible d'avoir plusieurs titres de niveau 1
- toujours avoir quelque chose entre deux titres 
- ne pas finir sur un titre
- ne pas sauter de niveau entre deux titres (un titre 3 ne peut pas se trouver en suivant d'un titre 1)


## Les limites de mon algo et ce que je n'ai pas eu le temps de faire

J'ai implémenté jusqu'à 3 niveaux de titres et je n'ai pas implémenté des listes de listes (par manque de temps hélas).
On pourrait comparer mon code à un "code spaguetti" car il y a des variables dans tout les sens et qu'il n'est pas très lisible malgrès ma division en fonctions. Il faut savoir aussi que je parcours mon fichier JSON créé pour supprimers les virgules en trop, dons en terme de complexité ce n'est pas terrible, pour des fichiers markdown qui contiennent peu de contenue cela reste correct, mais on peut imaginer que si on lance mon programme avec un fichier markdown conséquent cela prendrai quelques secondes pour que mon programmes finisse.

## Mes impressions

Je suis assez content de ce que j'ai fais car j'ai effectué pas mal de tests et je n'ai pas rencontré d'erreur sur ma dernière version de ce code (avec les conventions de markdown que je m'étais fixé, sans liste de liste par exemple), mais en voyant mon code qui fait 430 lignes je me dis que j'aurai mieux fait de prendre une feuille et un stylo et de réfléchir à l'algo sur papier, et voir même, prendre un jour entier pour cela. Cela m'aurait permis de mieux structurer mon code et d'avoir une implémentation plus "smart", comme par exemple utiliser un dictionnaire en python, parser mon fichier et écrire dans ce dictionnaire chaque ligne correspondant au bon objet. En faisant cela j'aurai eu un parseur assez neutre et j'aurai pu me reservir du code pour parser d'autres types de fichiers, car le problème de ce code c'est qu'il est spécifique et donc utile uniquement pour parser un fichier Markdown en JSON.