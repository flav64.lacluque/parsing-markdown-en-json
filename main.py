import sys
from enum import Enum


# TODO 3 : écrire beaucoup plus de test pour voir les limites de notre algo
# TODO : erreur traitée pour l'instant : commencer par un titre 1, toujours quelque chose entre 2 titres, vérifie que le fichier en entré est bien un .md

class Etat(Enum):
    ETAT_INITIAL = "ETAT_INITIAL",
    CODE = "CODE",
    PARAGRAPHE = "PARAGRAPHE",
    TITRE1 = "TITRE1",
    TITRE2 = "TITRE2",
    TITRE3 = "TITRE3",
    LISTE_A_PUCE = "LISTE_A_PUCE",
    FIN_DE_LISTE_A_PUCE = "FIN_DE_LISTE_A_PUCE",
    EST_LIGNE_VIDE = "EST_LIGNE_VIDE",
    DEBUT_CODE = "DEBUT_CODE",
    FIN_CODE = "FIN_CODE",
    AGAIN_TITRE2 = "AGAIN_TITRE2",
    AGAIN_TITRE3 = "AGAIN_TITRE3",
    IMAGE = "IMAGE",
    ERREUR = "ERREUR"


def charge_fichier(filePath):
    global strResult, fichier
    if filePath[len(filePath) - 3:len(filePath)] != ".md":
        print("Le fichier n'est pas un .md")

    else:
        strResult = ""
        fichier = open(filePath, "r")
        lignes = fichier.readlines()
        for ligne in lignes:
            strResult = strResult + ligne

    fichier.close()
    return strResult


def enleveVirguleEnTrop(pathFichierJson):
    strResult = ""
    boolMaybeVirguleEnTrop = False
    strLigneAvant = ""
    with open(pathFichierJson, "r") as f:
        for ligne in f:

            if len(ligne) > 2:
                if boolMaybeVirguleEnTrop:
                    if ligne[len(ligne) - 3] == "}":
                        strResult += strLigneAvant.replace(",", "")
                    elif ligne[len(ligne) - 3] == "]":
                        strResult += strLigneAvant.replace(",", "")
                    else:
                        strResult += strLigneAvant
                    boolMaybeVirguleEnTrop = False
            else:
                if ligne[0] != "{":
                    strResult += "  }\n"

            if len(ligne) > 2:
                if ligne[len(ligne) - 3] == "]" or ligne[len(ligne) - 3] == "}" or ligne[len(ligne) - 3] == "," or \
                        ligne[len(ligne) - 2] == ",":
                    boolMaybeVirguleEnTrop = True
                    strLigneAvant = ligne
                else:
                    strResult += ligne
            else:
                strResult += ligne
    f.close()
    newFichierJson = open(pathFichierJson, "w")
    newFichierJson.write(strResult)


def parseFile(fichier_en_string, currentState, outputFile):
    espace = "  "
    compteurParagraphe = 1
    bool_for_title1 = False
    bool_for_title2 = False
    bool_for_title3 = False
    texteEntreTitre = True
    compteurTitre2 = 1
    compteurTitre3 = 0
    compteurAccoladeFermante = 0
    compteurImage = 1
    if currentState == Etat.ETAT_INITIAL:
        outputFile.write("{\n" + espace)
        compteurAccoladeFermante += 1
    print(currentState)
    for ligne in fichier_en_string:
        currentState, bool_for_liste = determineEtat(ligne, currentState, bool_for_title2, bool_for_title3,
                                                     bool_for_title1, texteEntreTitre)
        print(currentState, bool_for_liste)
        if currentState == Etat.ERREUR:
            print("Le fichier Markdown est incorrect, le parsing est interrompu")
            outputFile.write(
                "Le fichier mardown mis en entre ne respect pas les conventions, de ce fait le parsing a ete interrompu")
            break
        elif currentState == Etat.TITRE1:
            ligne = verifCaractereSpecial(ligne)
            espace = ecrisTitre1(outputFile, ligne[2:len(ligne)], espace)
            bool_for_title1 = True
            texteEntreTitre = False
            compteurAccoladeFermante += 2
        elif currentState == Etat.AGAIN_TITRE2:
            ligne = verifCaractereSpecial(ligne)
            compteurTitre2 += 1
            espace, compteurAccoladeFermante = ecrisAgainTritre2(outputFile, ligne, espace, bool_for_title3,
                                                                 compteurTitre2, compteurAccoladeFermante)
            bool_for_title3 = False
            texteEntreTitre = False
        elif currentState == Etat.AGAIN_TITRE3:
            ligne = verifCaractereSpecial(ligne)
            compteurTitre3 += 1
            espace, compteurAccoladeFermante = ecrisAgainTritre3(outputFile, ligne, espace, compteurTitre3,
                                                                 compteurAccoladeFermante)
            texteEntreTitre = False
        elif currentState == Etat.PARAGRAPHE:
            ligne = verifCaractereSpecial(ligne)
            compteurParagraphe = ecrisParagraphe(outputFile, ligne, espace, compteurParagraphe)
            texteEntreTitre = True
        elif currentState == Etat.TITRE2:
            ligne = verifCaractereSpecial(ligne)
            compteurAccoladeFermante += 2
            bool_for_title2 = True
            texteEntreTitre = False
            espace = ecrisTitre2(outputFile, ligne[2:len(ligne)], espace, compteurTitre2)
        elif currentState == Etat.TITRE3:
            ligne = verifCaractereSpecial(ligne)
            compteurAccoladeFermante += 2
            bool_for_title3 = True
            compteurTitre3 += 1
            texteEntreTitre = False
            espace = ecrisTitre3(outputFile, ligne, espace, compteurTitre3)
        elif currentState == Etat.IMAGE:
            ecrisImage(outputFile, ligne, espace, compteurImage)
            compteurImage += 1
            texteEntreTitre = True
        elif currentState == Etat.DEBUT_CODE:
            ecrisDebutCode(outputFile, espace)
            texteEntreTitre = True
        elif currentState == Etat.CODE:
            ligne = verifCaractereSpecial(ligne)
            ecrisCode(outputFile, ligne)
        elif currentState == Etat.FIN_CODE:
            ecrisFinCode(outputFile)
        elif currentState == Etat.LISTE_A_PUCE:
            ligne = verifCaractereSpecial(ligne)
            texteEntreTitre = True
            espace = ecrisListeAPuce(outputFile, ligne, espace, bool_for_liste)
        elif currentState == Etat.FIN_DE_LISTE_A_PUCE:
            espace = ecrisFinListeAPuce(outputFile, espace)

    if currentState == Etat.LISTE_A_PUCE:
        espace = ecrisFinListeAPuce(outputFile, espace)
    elif not texteEntreTitre:
        print("Le fichier Markdown est incorrect, le parsing est interrompu")
        outputFile.write(
            "Le fichier mardown mis en entre ne respect pas les conventions, de ce fait le parsing a ete interrompu")
    else:
        j = 2
        for i in range(compteurAccoladeFermante + 1):
            stringPourLeNouvelEspace = ""
            for k in range(len(espace) - j):
                stringPourLeNouvelEspace += " "

            outputFile.write(stringPourLeNouvelEspace + "},\n")
            j += 2


def ecrisCrochetFermant(outputFile, espace):
    stringPourLeNouvelEspace = ""
    for i in range(len(espace) - 2):
        stringPourLeNouvelEspace += " "

    outputFile.write(stringPourLeNouvelEspace + "],\n")
    return stringPourLeNouvelEspace


def verifCaractereSpecial(ligne):
    result = ""
    for caractere in ligne:
        if caractere == "\"":
            result += "\\" + caractere
        else:
            result += caractere
    return result


def ecrisListeAPuce(outputFile, ligne, espace, firstInListeAPuce):
    ligne = ligne[2: len(ligne)]
    if firstInListeAPuce:
        outputFile.write(
            espace + "\"ul\": [\n"
        )
        espace += "  "
        outputFile.write(espace + "{\"li\": \"" + ligne + "\"}, \n")
    else:
        outputFile.write(espace + "{\"li\": \"" + ligne + "\"}, \n")
    return espace


def ecrisFinListeAPuce(outputFile, espace):
    stringPourLeNouvelEspace = ""
    for i in range(len(espace) - 2):
        stringPourLeNouvelEspace += " "
    outputFile.write(stringPourLeNouvelEspace + "],\n")
    return stringPourLeNouvelEspace


def ecrisFinCode(outputFile):
    outputFile.write("\",\n")


def ecrisCode(outputFile, ligne):
    outputFile.write(ligne + "\\n")


def ecrisDebutCode(outputFile, espace):
    outputFile.write(espace + "\"code\": \"")


def ecrisParagraphe(outputFile, ligne, espace, compteur_de_paragraphe):
    outputFile.write(espace + "\"p" + str(compteur_de_paragraphe) + "\": \"" + ligne + "\",\n")
    compteur_de_paragraphe += 1
    return compteur_de_paragraphe


def ecrisTitre1(outputFile, ligne, espace):
    espace += "  "
    outputFile.write("\"h1\": {\n" + espace + "\"label\": \"" + ligne + "\",\n" + espace + "\"content\": {\n")
    espace += "  "
    return espace


def ecrisTitre2(outputFile, ligne, espace, compteurTitre2):
    outputFile.write(
        espace + "\"h2_" + str(
            compteurTitre2) + "\": {\n" + espace + "  " + "\"label\": \"" + ligne + "\",\n" + espace + "  " + "\"content\": {\n")
    espace += "    "
    return espace


def ecrisTitre3(outputFile, ligne, espace, compteurTitre3):
    outputFile.write(
        espace + "\"h3_" + str(
            compteurTitre3) + "\": {\n" + espace + "  " + "\"label\": \"" + ligne + "\",\n" + espace + "  " + "\"content\": {\n")
    espace += "    "
    return espace


def ecrisAgainTritre2(outputFile, ligne, espace, estDansTitre3, compteurTitre2, compteurAccoladeFermante):
    global stringPourLeNouvelEspace, stringPourLeNouvelEspace2
    if estDansTitre3:
        j = 2
        for i in range(4):
            stringPourLeNouvelEspace = ""
            for i in range(len(espace) - j):
                stringPourLeNouvelEspace += " "
            compteurAccoladeFermante = compteurAccoladeFermante - 1
            outputFile.write(stringPourLeNouvelEspace + "},\n")

            j += 2
        stringPourLeNouvelEspace2 = ""
        for i in range(len(stringPourLeNouvelEspace)):
            stringPourLeNouvelEspace2 += " "
        outputFile.write(
            stringPourLeNouvelEspace2 + "\"h2_" + str(
                compteurTitre2) + "\": {\n" + stringPourLeNouvelEspace2 + "  " + "\"label\": \"" + ligne + "\",\n" + stringPourLeNouvelEspace2 + "  " + "\"content\": {\n")
        stringPourLeNouvelEspace2 += "    "
        compteurAccoladeFermante += 2
    else:
        j = 2
        for i in range(2):
            stringPourLeNouvelEspace = ""
            for i in range(len(espace) - j):
                stringPourLeNouvelEspace += " "

            outputFile.write(stringPourLeNouvelEspace + "},\n")
            compteurAccoladeFermante = compteurAccoladeFermante - 1
            stringPourLeNouvelEspace2 = ""
            j += 2
        for i in range(len(stringPourLeNouvelEspace)):
            stringPourLeNouvelEspace2 += " "
        outputFile.write(
            stringPourLeNouvelEspace2 + "\"h2_" + str(
                compteurTitre2) + "\": {\n" + stringPourLeNouvelEspace2 + "  " + "\"label\": \"" + ligne + "\",\n" + stringPourLeNouvelEspace2 + "  " + "\"content\": {\n")
        stringPourLeNouvelEspace2 += "    "
        compteurAccoladeFermante += 2
    return stringPourLeNouvelEspace2, compteurAccoladeFermante


def ecrisAgainTritre3(outputFile, ligne, espace, compteurTitre3, compteurAccoladeFermante):
    global stringPourLeNouvelEspace
    j = 2
    for i in range(2):
        stringPourLeNouvelEspace = ""
        for i in range(len(espace) - j):
            stringPourLeNouvelEspace += " "

        outputFile.write(stringPourLeNouvelEspace + "},\n")
        compteurAccoladeFermante = compteurAccoladeFermante - 1
        j += 2
    stringPourLeNouvelEspace2 = ""
    for i in range(len(stringPourLeNouvelEspace)):
        stringPourLeNouvelEspace2 += " "
    outputFile.write(
        stringPourLeNouvelEspace2 + "\"h3_" + str(
            compteurTitre3) + "\": {\n" + stringPourLeNouvelEspace2 + "  " + "\"label\": \"" + ligne + "\",\n" + stringPourLeNouvelEspace2 + "  " + "\"content\": {\n")
    stringPourLeNouvelEspace2 += "    "
    compteurAccoladeFermante += 2
    return stringPourLeNouvelEspace2, compteurAccoladeFermante


def ecrisImage(outputFile, ligne, espace, compteurImage):
    strInfo = ""
    strChemin = ""
    strTitre = ""
    boolForInfo = False
    boolForChemin = False
    boolForTittre = False
    for lettre in ligne:

        if boolForTittre:
            if lettre == "\"":
                boolForTittre = False
            else:
                strTitre += lettre
        if boolForChemin:
            if lettre == ")":
                boolForChemin = False
            elif lettre == "\"":
                boolForChemin = False
                boolForTittre = True
            else:
                strChemin += lettre
        if boolForInfo:
            if lettre == "]":
                boolForInfo = False
            else:
                strInfo += lettre
        if lettre == "[":
            boolForInfo = True
        if lettre == "(":
            boolForChemin = True
    outputFile.write(
        espace + "\"image" + str(compteurImage) + "\": {\n" +
        espace + "  \"info\": \"" + strInfo + "\",\n" +
        espace + "  \"chemin de l'image\": \"" + strChemin + "\",\n" +
        espace + "  \"titre de l'image\": \"" + strTitre + "\"\n" +
        espace + "},\n"
    )


def determineEtat(ligne, currentState, estDejaPasseParUnTitre2, estDejaPasseParUnTitre3, estDejaPasseParUnTitre1,
                  texteEntreTitre):
    if len(ligne) == 0:
        if currentState == Etat.LISTE_A_PUCE:
            return Etat.FIN_DE_LISTE_A_PUCE, False
        else:
            return Etat.EST_LIGNE_VIDE, False
    else:
        if currentState == Etat.DEBUT_CODE:
            return Etat.CODE, False
        elif ligne[0] == "#" and not (currentState == Etat.CODE):
            if ligne[1] == "#" and not (currentState == Etat.CODE):
                if ligne[2] == "#" and not (currentState == Etat.CODE):
                    if estDejaPasseParUnTitre3:
                        return Etat.AGAIN_TITRE3, False
                    elif currentState == Etat.ETAT_INITIAL or not texteEntreTitre or not estDejaPasseParUnTitre2:
                        return Etat.ERREUR, False
                    else:
                        return Etat.TITRE3, False
                if not estDejaPasseParUnTitre1 or not texteEntreTitre:
                    return Etat.ERREUR, False
                elif estDejaPasseParUnTitre2:
                    return Etat.AGAIN_TITRE2, False
                elif currentState == Etat.ETAT_INITIAL:
                    return Etat.ERREUR, False
                else:
                    return Etat.TITRE2, False
            if estDejaPasseParUnTitre2 or estDejaPasseParUnTitre1 or not texteEntreTitre:
                return Etat.ERREUR, False
            else:
                if estDejaPasseParUnTitre1:
                    return Etat.ERREUR, False
                return Etat.TITRE1, False
        elif (ligne[0] == "`") and (ligne[1] == "`") and (ligne[2] == "`") and not (currentState == Etat.CODE):
            return Etat.DEBUT_CODE, False
        elif (ligne[0] == "`") and (ligne[1] == "`") and (ligne[2] == "`") and (currentState == Etat.CODE):
            return Etat.FIN_CODE, False
        elif currentState == Etat.DEBUT_CODE:
            return Etat.CODE, False
        elif currentState == Etat.CODE:
            return Etat.CODE, False
        elif ((ligne[0] == "*") or (ligne[0] == "-")) and not (currentState == Etat.CODE):
            if currentState == Etat.EST_LIGNE_VIDE or currentState == Etat.PARAGRAPHE:
                return Etat.LISTE_A_PUCE, True
            else:
                return Etat.LISTE_A_PUCE, False
        elif (ligne[0] == "!"):
            return Etat.IMAGE, False
        elif ligne[0].isalpha() and not (currentState == Etat.CODE):
            return Etat.PARAGRAPHE, False


def main():
    args = sys.argv[1:]
    pathMD = args[0]
    newSTR = pathMD.split("\\")
    newSTRSplit = newSTR[-1].split(".")
    nomFichierJSON = newSTRSplit[0] + ".json"
    fichier_en_string = charge_fichier(pathMD)
    newFichier = fichier_en_string.split('\n')
    currentState = Etat.ETAT_INITIAL
    pathJSON = nomFichierJSON
    outputFile = open(pathJSON, "w")
    parseFile(newFichier, currentState, outputFile)
    outputFile.close()

    enleveVirguleEnTrop(pathJSON)


main()

